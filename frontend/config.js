export default {
  colors: {
    default: '#344675',
    primary: '#42b883',
    info: '#1d8cf8',
    danger: '#fd5d93',
    orange: '#ff8a76',
    teal: '#00d6b4',
    primaryGradient: [
      'rgba(76, 211, 150, 0.2)',
      'rgba(53, 183, 125, 0)',
      'rgba(119,52,169,0)'
    ],
    purpleGradient: [
      'rgba(199, 58, 177,0.2)',
      'rgba(222, 104, 203,0)',
      'rgba(119,52,169,0)'
    ],
    orangeGradient: [
      'rgba(245, 121, 100, 0.2)',
      'rgba(250, 150, 132,0)',
      'rgba(119,52,169,0)'
    ],
    blueGradient: [
      'rgba(19, 131, 240,0.2)',
      'rgba(57, 155, 250,0)',
      'rgba(119,52,169,0)'
    ]
  },
  MAPS_API_KEY: process.env.GOOGLE_API_KEY
}
