import BaseAlert from './BaseAlert.vue'
import BaseInput from './Inputs/BaseInput.vue'
import BaseTextarea from './Inputs/BaseTextarea.vue'
import BaseSwitch from './BaseSwitch.vue'
import Badge from './Badge'
import BaseProgress from './BaseProgress.vue'
import BaseButton from './BaseButton.vue'

import BaseDropdown from './BaseDropdown.vue'

import Card from './Cards/Card.vue'
import StatsCard from './Cards/StatsCard.vue'
import BaseNav from './Navbar/BaseNav'
import NavbarToggleButton from './Navbar/NavbarToggleButton'

import ImageUpload from './ImageUpload'
import TabPane from './Tabs/Tab.vue'
import Tabs from './Tabs/Tabs.vue'
import Modal from './Modal.vue'
import Slider from './Slider.vue'
import LoadingPanel from './LoadingPanel.vue'

import BasePagination from './BasePagination.vue'

import SidebarPlugin from './SidebarPlugin'

import AnimatedNumber from './AnimatedNumber'

export {
  BaseSwitch,
  Badge,
  BaseAlert,
  BaseProgress,
  BasePagination,
  BaseInput,
  BaseTextarea,
  Card,
  StatsCard,
  BaseDropdown,
  ImageUpload,
  SidebarPlugin,
  BaseNav,
  NavbarToggleButton,
  TabPane,
  Tabs,
  Modal,
  Slider,
  AnimatedNumber,
  BaseButton,
  LoadingPanel
}
