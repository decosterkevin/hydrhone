# nuxt-content-article

Hydrhone est une application web permettant de visualiser en un cout d'œil la météo et les données hydrométriques du Rhone au alentour de Genève. 

Click here to view the [demo](https://hydrhone.ch/)

## Build Setup

```bash
# install dependencies
$ yarn

# serve with hot reload at localhost:3000
$ yarn dev

# generate static project
$ yarn generate

# view a production version of your app
$ yarn start
```
