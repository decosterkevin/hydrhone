export const state = () => ({
  darkMode: true,
  sidebarColor: 'blue'
})

export const mutations = {
  setDarkMode(state, value) {
    state.darkMode = value
  },
  setSidebarColor(state, value) {
    state.sidebarColor = value
  }
}
