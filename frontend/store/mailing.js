export const state = () => ({})

export const mutations = {}
export const actions = {
  subscribe({ commit }, params) {
    return this.$axios.$post(`/mailing/subscribe/`, params)
  },
  contact({ commit }, params) {
    return this.$axios.$post(`mailing/contact/`, params)
  }
}
