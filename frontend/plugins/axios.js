require('dotenv').config()
export default function ({ $axios, store }) {
  $axios.onRequest((config) => {
    config.headers.common.Authorization = `Token ${process.env.API_TOKEN}`
  })
}
