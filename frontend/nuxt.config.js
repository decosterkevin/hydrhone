import MomentLocalesPlugin from 'moment-locales-webpack-plugin'
const createSitemapRoutes = async () => {
  const routes = []
  let articles = []
  const { $content } = require('@nuxt/content')
  if (articles === null || articles.length === 0)
    articles = await $content('articles').fetch()
  for (const article of articles) {
    routes.push(`blog/${article.slug}`)
  }
  return routes
}
export default {
  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
   */
  /*
   ** Headers of the page
   ** See https://nuxtjs.org/api/configuration-head
   
   */
  target: 'server',
  server: {
    host: '0.0.0.0' // default: localhost
  },
  ssr: true,
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content:
          'initial-scale=1, width=device-width, height=device-height, viewport-fit=cover'
      },
      {
        hid: 'hydrhone.ch',
        name: 'hydrhone.ch',
        content:
          "Hydrhone est une application web permettant de visualiser en un cout d'œil la météo et les données hydrométriques du Rhone au alentour de Genève. "
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' }
    ],
    bodyAttrs: {
      class: 'sidebar-mini' // delete the class to have the sidebar expanded by default. Add `white-content` class here to enable "white" mode.
    }
  },
  router: {
    linkExactActiveClass: 'active'
  },
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: [
    'assets/css/demo.css',
    'assets/css/nucleo-icons.css',
    'assets/sass/black-dashboard-pro.scss',
    'assets/css/weather-icons.css',
    "@fortawesome/fontawesome-svg-core/styles.css"
  ],
  /*

   ** Plugins to load before mounting the App
   ** https://nuxtjs.org/guide/plugins
   */
  // plugins: ['~/plugins/i18n.js', '~/plugins/fontawesome.js'],
  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // // Doc: https://github.com/nuxt-community/eslint-module
    // '@nuxtjs/eslint-module',
    // // Doc: https://github.com/nuxt-community/stylelint-module
    // '@nuxtjs/stylelint-module',
    '@nuxtjs/pwa',
    '@nuxt/image'
    // 'nuxt-purgecss',
  ],
  plugins: [
    `~/plugins/dashboard-plugin.js`,
    `~/plugins/vue-content-placeholders`,
    { src: '~/plugins/pwa-update.js', mode: 'client' },
    { src: '~/plugins/vue-gtag.js', mode: 'client' },
    "~/plugins/fontawesome.js",
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://github.com/nuxt/content
    '@nuxtjs/axios',
    '@nuxtjs/dotenv',
    '@nuxt/content',
    '@nuxtjs/sitemap',
    'nuxt-i18n',
    '@nuxtjs/dayjs',
    'nuxt-breakpoints',
    'nuxt-helmet',
    'nuxt-webfontloader',
    '@nuxtjs/robots'
  ],
  sitemap: {
    hostname: process.env.BASE_URL,
    gzip: true,
    routes: createSitemapRoutes
  },
  robots: {
    UserAgent: '*',
    Allow: '/',
    Sitemap: `${process.env.BASE_URL}/sitemap.xml`
  },
  image: {
    screens: {
      xs: 320,
      sm: 640,
      md: 768,
      lg: 1024,
      xl: 1280,
      xxl: 1536,
      '2xl': 1536
    },
    twicpics: {
      baseURL: process.env.TWICPICS_URL
    }
  },
  webfontloader: {
    google: {
      families: ['Poppins:200,300,400,600,700,800&display=swap'] // Loads Lato font with weights 400 and 700
    }
  },
  dayjs: {
    locales: ['en', 'fr'],
    defaultLocale: 'fr',
    defaultTimeZone: 'Europe/Paris',
    plugins: [
      'utc', // import 'dayjs/plugin/utc'
      'timezone', // import 'dayjs/plugin/timezone'
      'relativeTime',
      'localizedFormat'
    ] // Your Day.js plugin
  },
  i18n: {
    defaultLocale: 'fr',
    strategy: 'no_prefix',
    langDir: 'locales',
    locales: [
      {
        code: 'en',
        iso: 'en-US',
        file: 'en.js'
      },
      {
        code: 'fr',
        iso: 'fr-FR',
        file: 'fr.js'
      }
    ]
  },
  axios: {
    baseURL: process.env.API_BASE_URL + '/api',
    headers: {
      common: {
        Authorization: `Token ${process.env.API_TOKEN}`
      }
    }
  },
  pwa: {
    manifest: {
      lang: 'fr',
      icon: {
        source: 'assets/svg/logo.svg'
      },
      theme_color: '#1f85f7'
    }
  },
  /*
   ** Content module configuration
   ** See https://content.nuxtjs.org/configuration
   */
  content: {
    markdown: {
      prism: {
        theme: 'prism-themes/themes/prism-material-oceanic.css'
      }
    },
    nestedProperties: ['author.name']
  },
  fontawesome: {
    icons: {
      solid: [
        'faExpand',
        'faTemperatureLow',
        'faMapMarkedAlt',
        'faThermometer',
        'faThermometerHalf',
        'faTemperatureHigh',
        'faThermometerQuarter',
        'faWater',
        'faTachometerAlt',
        'faWind',
        'faCloud',
        'faTint',
        'faCrosshairs',
        'faEnvelope',
        'faLevelUpAlt'
      ],
      regular: ['faSun', 'faMoon']
    }
  },
  build: {
    transpile: [/^element-ui/],
    /*
     ** You can extend webpack config here
     */
    plugins: [new MomentLocalesPlugin()],
    extend(config, ctx) {},
    babel: {
      plugins: [
        [
          'component',
          {
            libraryName: 'element-ui',
            styleLibraryName: 'theme-chalk'
          }
        ]
      ]
    }
  }
}
