from django.urls import path, include
from rest_framework.routers import DefaultRouter
from water import views

router = DefaultRouter()
router.register(r'sources', views.SourceViewSet, basename="sources")
router.register(r'datapoints', views.UnifiedDataPointViewSet, basename="datapoints")
router.register(r'temperatures', views.TemperatureDataViewSet, basename="temperatures")
router.register(r'levels', views.LevelDataViewSet, basename="levels")
router.register(r'discharges', views.DischargeDataViewSet, basename="discharges")
router.register(r'discharge_probabilistic_forecasts', views.DischargeDeterministicForecastDataViewSet, basename="discharge_probabilistic_forecasts")
router.register(r'discharge_deterministic_forecasts', views.DischargeProbabilisticForecastDataViewSet, basename="discharge_deterministic_forecasts")

source_router = DefaultRouter()
source_router.register(r'datapoints', views.UnifiedDataPointSourceViewSet, basename="sources-datapoints")
source_router.register(r'temperatures', views.TemperatureDataSourceViewSet, basename="sources-temperatures")
source_router.register(r'levels', views.LevelDataSourceViewSet, basename="sources-levels")
source_router.register(r'discharges', views.DischargeDataSourceViewSet, basename="sources-discharges")
source_router.register(r'discharge_probabilistic_forecasts', views.DischargeProbabilisticForecastDataSourceViewSet, basename="sources-discharge_probabilistic_forecasts")
source_router.register(r'discharge_deterministic_forecasts', views.DischargeDeterministicForecastDataSourceViewSet, basename="sources-discharge_deterministic_forecasts")

urlpatterns = [
   path('', include(router.urls)),
   path('sources/<int:source_id>/', include(source_router.urls)),
]