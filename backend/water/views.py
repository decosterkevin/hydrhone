from water.serializers import SourceSerializer, TemperatureDataSerializer,UnifiedDataPointSerializer, LevelDataSerializer, DischargeDataSerializer, DischargeDeterministicForecastDataSerializer, DischargeProbabilisticForecastDataSerializer
from water.models import Source, TemperatureData, LevelData, DischargeData, DischargeDeterministicForecastData, DischargeProbabilisticForecastData
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.generics import  RetrieveAPIView, ListAPIView
from rest_framework.permissions import AllowAny, AllowAny
from django.db.models.functions import Trunc
from rest_framework import viewsets
from django.db.models import Subquery, FloatField, Avg, ExpressionWrapper, OuterRef, F
from rest_framework import filters


class SourceViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = SourceSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ["station_id", "title", "is_active"]
    def get_queryset(self):
        return Source.objects.annotate(
            last_temperature = Subquery(
                TemperatureData.objects.filter(source__id=OuterRef("pk")).values("value")[:1]
            ),
            last_level = Subquery(
                LevelData.objects.filter(source__id=OuterRef("pk")).values("value")[:1]
            ),
            last_discharge = Subquery(
                DischargeData.objects.filter(source__id=OuterRef("pk")).values("value")[:1]
            ),
            last_update = Subquery(
                TemperatureData.objects.filter(source__id=OuterRef("pk")).values("datetime")[:1]
            )

        )
class UnifiedDataPointViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = UnifiedDataPointSerializer
    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]
    ordering_fields = ["datetime"]
    queryset = TemperatureData.objects.all()
    filterset_fields = {
        'datetime':['gte', 'lte', 'exact', 'gt', 'lt'],
    }
    def get_queryset(self):
        temperature_qs = TemperatureData.objects.annotate(
            hour=Trunc('datetime', 'hour'),
            level = LevelData.objects.filter(source__id=OuterRef("source"), datetime__lte=OuterRef("datetime")).order_by('-datetime').values("value")[:1],
            discharge = DischargeData.objects.filter(source__id=OuterRef("source"), datetime__lte=OuterRef("datetime")).order_by('-datetime').values("value")[:1]
        )
        return temperature_qs.values("hour").annotate(
            temperature = Avg("value"),
            level = Avg("level"),
            discharge = Avg("discharge"),
            datetime=F("hour"),
            source_id=F("source__id")
        )
class UnifiedDataPointSourceViewSet(UnifiedDataPointViewSet):
    serializer_class = UnifiedDataPointSerializer
    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]
    ordering_fields = ["datetime"]
    queryset = TemperatureData.objects.all()
    filterset_fields = {
        'datetime':['gte', 'lte', 'exact', 'gt', 'lt'],
    }
class TemperatureDataViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = TemperatureDataSerializer
    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]
    ordering_fields = ["datetime"]
    queryset = TemperatureData.objects.all()
    filterset_fields = {
        "value": ['exact'], 
        'datetime':['gte', 'lte', 'exact', 'gt', 'lt'],
    }

class TemperatureDataSourceViewSet(TemperatureDataViewSet):
    def get_queryset(self):
        source_id = self.kwargs['source_id']
        return super().get_queryset().filter(source__id=source_id)

class LevelDataViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = LevelDataSerializer
    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]
    ordering_fields = ["datetime"]
    queryset = LevelData.objects.all()
    filterset_fields = {
        "value": ['exact'], 
        'datetime':['gte', 'lte', 'exact', 'gt', 'lt'],
    }

class LevelDataSourceViewSet(LevelDataViewSet):
    def get_queryset(self):
        source_id = self.kwargs['source_id']
        return super().get_queryset().filter(source__id=source_id)

class DischargeDataViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = DischargeDataSerializer
    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]
    ordering_fields = ["datetime"]
    queryset = DischargeData.objects.all()
    filterset_fields = {
        "value": ['exact'], 
        'datetime':['gte', 'lte', 'exact', 'gt', 'lt'],
    }

class DischargeDataSourceViewSet(DischargeDataViewSet):
    filterset_fields = {
        "value": ['exact'], 
        'datetime':['gte', 'lte', 'exact', 'gt', 'lt'],
    }
    def get_queryset(self):
        source_id = self.kwargs['source_id']
        return super().get_queryset().filter(source__id=source_id)

class DischargeDeterministicForecastDataViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = DischargeDeterministicForecastDataSerializer
    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]
    ordering_fields = ["datetime"]
    queryset = DischargeDeterministicForecastData.objects.all()
    filterset_fields = {
        "value": ['exact'], 
        'datetime':['gte', 'lte', 'exact', 'gt', 'lt'],
    }
class DischargeDeterministicForecastDataSourceViewSet(DischargeDeterministicForecastDataViewSet):
    def get_queryset(self):
        source_id = self.kwargs['source_id']
        return super().get_queryset().filter(source__id=source_id)

class DischargeProbabilisticForecastDataViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = DischargeProbabilisticForecastDataSerializer
    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]
    ordering_fields = ["datetime"]
    queryset = DischargeProbabilisticForecastData.objects.all()
    filterset_fields = {
        "value": ['exact'], 
        'datetime':['gte', 'lte', 'exact', 'gt', 'lt'],
    }

class DischargeProbabilisticForecastDataSourceViewSet(DischargeProbabilisticForecastDataViewSet):
    def get_queryset(self):
        source_id = self.kwargs['source_id']
        return super().get_queryset().filter(source__id=source_id)