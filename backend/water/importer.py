import pandas as pd
import requests
import numpy as np

class HydrotatenProvider:

    @staticmethod
    def fetch_csv_to_list(url):
        try:
            df = pd.read_csv(url).rename(columns={"Datetime": "datetime", "Time": "datetime"}).set_index("datetime")
            df.index = pd.to_datetime(df.index)
            df.columns = ["value"]
            return df.reset_index().to_dict('records')
        except Exception as exception:
            print(exception)
            return list()

    @staticmethod
    def fetch_json_to_list(url, x_record_path="x", y_record_path="y", data_path="plot.data", extra_header_kwargs=None):
        if extra_header_kwargs is None:
            extra_header_kwargs = {}
        headers = {
            'user-agent': 'hydrhone',
            'Content-Type': 'application/json',
            "Accept": "*/*",
            "Accept-Encoding": "gzip, deflate, br",
            "Accept-Language": "en-US,en;q=0.9,fr;q=0.8",
            "Cache-Control": "no-cache",
            "Connection": "keep-alive",
            "Pragma": "no-cache",
            "sec-ch-ua": 'Chromium";v="110", "Not A(Brand";v="24", "Google Chrome";v="110"',
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": '"Linux"',
            "Sec-Fetch-Dest": "empty",
            "Sec-Fetch-Mode": "cors",
            "Sec-Fetch-Site": "same-origin",
            "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36",
            **extra_header_kwargs
        }
        response = requests.get(url, headers=headers)
        if response.status_code == requests.codes.ok:
            r_json = response.json()
            for attr in data_path.split("."):
                r_json = r_json[attr]
            x = pd.json_normalize(r_json, x_record_path).rename(columns={0:"datetime"})
            y = pd.json_normalize(r_json, y_record_path, ["name"]).rename(columns={0:"value"})
            df = pd.concat([x,y], axis=1)
            df = pd.pivot_table(df, index="datetime", values="value", columns="name", aggfunc="mean")
            df.index = pd.to_datetime(df.index)
            
            return df.replace([np.inf, -np.inf, np.nan], None).reset_index().to_dict('records')
                
        return []
    

