
from rest_framework import serializers
from water.models import Source, TemperatureData, LevelData, DischargeData, DischargeDeterministicForecastData, DischargeProbabilisticForecastData

class SourceSerializer(serializers.ModelSerializer):
    last_temperature = serializers.FloatField()
    last_discharge = serializers.FloatField()
    last_level = serializers.FloatField()
    last_update = serializers.DateTimeField()
    class Meta:
        model = Source
        fields = ("id", "title", "station_id", "coordinate", "available_datatype", "is_active", "is_primary", "last_temperature",  "last_discharge",  "last_level", "last_update", "nd_ranges")

class UnifiedDataPointSerializer(serializers.ModelSerializer):
    temperature = serializers.FloatField()
    discharge = serializers.FloatField(read_only=True, default=None)
    level = serializers.FloatField(read_only=True, default=None )
    source_id = serializers.IntegerField(read_only=True)
    class Meta:
        model = TemperatureData
        fields = ("id", "temperature", "level", "discharge", "datetime", "source_id")

class TemperatureDataSerializer(serializers.ModelSerializer):
    source = serializers.PrimaryKeyRelatedField(read_only=True)
    class Meta:
        model = TemperatureData
        fields = ("id", "value", "datetime", "source")

class LevelDataSerializer(serializers.ModelSerializer):
    source = serializers.PrimaryKeyRelatedField(read_only=True)
    class Meta:
        model = LevelData
        fields = ("id", "value", "datetime", "source")

class DischargeDataSerializer(serializers.ModelSerializer):
    source = serializers.PrimaryKeyRelatedField(read_only=True)
    class Meta:
        model = DischargeData
        fields = ("id", "value", "datetime", "source")

class DischargeDeterministicForecastDataSerializer(serializers.ModelSerializer):
    source = serializers.PrimaryKeyRelatedField(read_only=True)
    class Meta:
        model = DischargeDeterministicForecastData
        fields = ("id", "value", "datetime", "source")

class DischargeProbabilisticForecastDataSerializer(serializers.ModelSerializer):
    source = serializers.PrimaryKeyRelatedField(read_only=True)
    class Meta:
        model = DischargeProbabilisticForecastData
        fields = ("id", "value", "datetime", "source")
