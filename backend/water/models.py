from django.db import models
from django.contrib.gis.db.models import PointField
from django.contrib.gis.geos import Point
from django.db.models.signals import post_save
from django.contrib.gis.db.models import PointField
from multiselectfield import MultiSelectField
from celery import shared_task
from dynamic_preferences.registries import global_preferences_registry
from water.importer import HydrotatenProvider
from django.apps import apps
from django_better_admin_arrayfield.models.fields import ArrayField
from django.db.models.signals import post_migrate
from django.dispatch import receiver

class Source(models.Model):
    class Type(models.TextChoices):
        TEMPERATURE = "TEMPERATURE", "TemperatureData"
        LEVEL = "LEVEL", "LevelData"
        DISCHARGE = "DISCHARGE", "DischargeData"
        DETERMINISTIC_FORECAST = "DETERMINISTIC_FORECAST", "DischargeDeterministicForecastData"
        PROBABILISTIC_FORECAST = "PROBABILISTIC_FORECAST", "DischargeProbabilisticForecastData"

    title = models.CharField(max_length=256)
    station_id = models.IntegerField(unique=True) 
    coordinate = PointField(blank=True, null=True)
    is_active = models.BooleanField(default=True)
    is_primary = models.BooleanField(default=False)
    available_datatype = ArrayField(models.CharField(choices=Type.choices, max_length=64), default=[Type.TEMPERATURE, Type.LEVEL, Type.DISCHARGE, Type.DETERMINISTIC_FORECAST, Type.PROBABILISTIC_FORECAST])


    urls = ArrayField(models.CharField(max_length=1028), default=list)
    field_map = models.JSONField(default=dict)
    source_kwargs = models.JSONField(default=dict)
    nd_ranges = ArrayField(models.IntegerField(), size=4, default=list)

    def save(self, *args, **kwargs):
        if self.is_primary:
            Source.objects.exclude(id=self.id).update(is_primary=False)
        super().save(*args, **kwargs)
    
    def __str__(self):
        return f'{self.title} ({self.station_id})'

    class Meta:
        verbose_name = "Source"
        verbose_name_plural = "Sources"

    @classmethod
    def get_dataclasses(cls):
        return [TemperatureData, LevelData, DischargeData, DischargeDeterministicForecastData, DischargeProbabilisticForecastData]

class TemperatureData(models.Model):
    source = models.ForeignKey("water.Source", related_name="temperatures", on_delete=models.CASCADE)
    value = models.FloatField()
    datetime = models.DateTimeField(blank=True, null=True)
    class Meta:
        unique_together = ["source", "datetime"]
        verbose_name = "Temperature"
        verbose_name_plural = "Temperatures"
        ordering = ['-datetime']
    


class LevelData(models.Model):
    source = models.ForeignKey("water.Source", related_name="levels", on_delete=models.CASCADE)
    value = models.FloatField()
    datetime = models.DateTimeField(blank=True, null=True)
    class Meta:
        unique_together = ["source", "datetime"]
        verbose_name = "Level"
        verbose_name_plural = "Levels"
        ordering = ['-datetime']

class DischargeData(models.Model):
    source = models.ForeignKey("water.Source", related_name="discharges", on_delete=models.CASCADE)
    value = models.FloatField()
    datetime = models.DateTimeField(blank=True, null=True)
    class Meta:
        unique_together = ["source", "datetime"]
        verbose_name = "Discharge"
        verbose_name_plural = "Discharges"
        ordering = ['-datetime']

class DischargeDeterministicForecastData(models.Model):
    source = models.ForeignKey("water.Source", related_name="discharge_deterministic_forecasts", on_delete=models.CASCADE)
    value = models.FloatField()
    datetime = models.DateTimeField(blank=True, null=True)

    class Meta:
        unique_together = ["source", "datetime"]
        verbose_name = "Deterministic Forecast"
        verbose_name_plural = "Deterministic Forecasts"
        ordering = ['-datetime']

class DischargeProbabilisticForecastData(models.Model):
    source = models.ForeignKey("water.Source", related_name="discharge_probabilistic_forecasts", on_delete=models.CASCADE)
    value = models.FloatField()
    datetime = models.DateTimeField(blank=True, null=True)
    class Meta:
        unique_together = ["source", "datetime"]
        verbose_name = "Probabilistic Forecast"
        verbose_name_plural = "Probabilistic Forecasts"
        ordering = ['-datetime']


@shared_task
def import_source_and_create_objects(source_id):
    source = Source.objects.get(id=source_id)
    for url in source.urls:
        if url.endswith(".csv"):
            objs = HydrotatenProvider.fetch_csv_to_list(url)
        else:
            objs = HydrotatenProvider.fetch_json_to_list(url, **source.source_kwargs)
        for obj in objs:
            for k,v in obj.items():
                if model_name := source.field_map.get(k, None):
                    model = apps.get_model(f"water.{model_name}")
                    model.objects.update_or_create(source=source, datetime=obj['datetime'], defaults={"value": v})