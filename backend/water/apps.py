from django.apps import AppConfig
from django.db.models.signals import post_migrate
from django.dispatch import receiver
from django.contrib.gis.geos import Point

FIELD_MAP = {
    "Médiane": "DischargeDeterministicForecastData",
    "Débit": "DischargeData",
    "Niveau d'eau": "LevelData",
    "Température": "TemperatureData"
}
class WaterConfig(AppConfig):
    name = 'water'

    def ready(self):
        @receiver(post_migrate)
        def _post_migrate(sender,**kwargs):
            from water.models import Source
            Source.objects.update_or_create(
                station_id=2174,
                defaults={
                    "title": "Rhône - Chancy, Aux Ripes",
                    "coordinate":  Point(6.084854131836, 46.204546),
                    "is_active": True,
                    "is_primary": True,
                    "available_datatype": ['TEMPERATURE', 'LEVEL', 'DISCHARGE', 'DETERMINISTIC_FORECAST', 'PROBABILISTIC_FORECAST'],
                    "field_map": FIELD_MAP,
                    "urls": [
                        "https://www.hydrodaten.admin.ch/plots/p_q_40days/2174_p_q_40days_fr.json",
                        "https://www.hydrodaten.admin.ch/plots/temperature_7days/2174_temperature_7days_fr.json",
                        "https://www.hydrodaten.admin.ch/plots/q_forecast/2174_q_forecast_fr.json",
                        
                    ],
                    "nd_ranges": [970, 1250, 1350, 1500],
                    "source_kwargs": {
                        "x_record_path": "x",
                        "y_record_path": "y",
                        "data_path": "plot.data",
                        "extra_header_kwargs": {
                            "Host": "www.hydrodaten.admin.ch",
                            "Referer": "https://www.hydrodaten.admin.ch/fr/seen-und-fluesse/stationen-und-daten/2174",
                        }
                    }
                }
            )
            Source.objects.update_or_create(
                station_id=2170,
                defaults={
                    "title": "Arve - Genève, Bout du Monde",
                    "coordinate":  Point(6.1627883969727, 46.186349894043),
                    "is_active": True,
                    "is_primary": False,
                    "available_datatype": ['TEMPERATURE', 'LEVEL', 'DISCHARGE', 'DETERMINISTIC_FORECAST', 'PROBABILISTIC_FORECAST'],
                    "field_map": FIELD_MAP,
                    "urls": [
                        "https://www.hydrodaten.admin.ch/plots/p_q_40days/2170_p_q_40days_fr.json",
                        "https://www.hydrodaten.admin.ch/plots/temperature_7days/2170_temperature_7days_fr.json",
                        "https://www.hydrodaten.admin.ch/plots/q_forecast/2170_q_forecast_fr.json",
                        
                    ],    
                    "nd_ranges": [490, 660, 740, 820],
                    "source_kwargs": {
                        "x_record_path": "x",
                        "y_record_path": "y",
                        "data_path": "plot.data",
                        "extra_header_kwargs": {
                            "Host": "www.hydrodaten.admin.ch",
                            "Referer": "https://www.hydrodaten.admin.ch/fr/seen-und-fluesse/stationen-und-daten/2174",
                        }
                    }
                }
            )
            Source.objects.update_or_create(
                station_id=2606,
                defaults={
                    "title": "Rhône - Genève, Halle de l 'île",
                    "coordinate":  Point(6.1390991269532, 46.208322550293),
                    "is_active": True,
                    "is_primary": False,
                    "available_datatype": ['TEMPERATURE', 'LEVEL', 'DISCHARGE', 'DETERMINISTIC_FORECAST', 'PROBABILISTIC_FORECAST'],
                    "field_map": FIELD_MAP,
                    "urls": [
                        "https://www.hydrodaten.admin.ch/plots/p_q_40days/2606_p_q_40days_fr.json",
                        "https://www.hydrodaten.admin.ch/plots/temperature_7days/2606_temperature_7days_fr.json",
                        "https://www.hydrodaten.admin.ch/plots/q_forecast/2606_q_forecast_fr.json",
                        
                    ],     
                    "nd_ranges": [300, 400, 450, 500],
                    "source_kwargs": {
                        "x_record_path": "x",
                        "y_record_path": "y",
                        "data_path": "plot.data",
                        "extra_header_kwargs": {
                            "Host": "www.hydrodaten.admin.ch",
                            "Referer": "https://www.hydrodaten.admin.ch/fr/seen-und-fluesse/stationen-und-daten/2174",
                        }
                    }
                }
            )