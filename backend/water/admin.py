from django.contrib import admin
from django.contrib.gis import admin as admin_gis
from .models import Source, TemperatureData, LevelData, DischargeData, DischargeDeterministicForecastData, DischargeProbabilisticForecastData
from django_better_admin_arrayfield.admin.mixins import DynamicArrayMixin
from water.tasks import import_source_and_create_objects

class LocationAdmin(admin_gis.GeoModelAdmin):
    default_lat=46.204546
    default_lon=6.143219

def reprocess_import_source(modeladmin, request, queryset):
    for source in queryset:
        import_source_and_create_objects.delay(source.id)
    

@admin.register(Source)
class SourceModelAdmin(LocationAdmin,DynamicArrayMixin):
    list_display = ['title', 'station_id', "is_primary"]
    search_fields = ("title", "station_id")

    actions = [reprocess_import_source]
    
@admin.register(TemperatureData)
class TemperatureDataModelAdmin(admin.ModelAdmin):
    list_display = ['datetime', 'value', 'source']
    autocomplete_fields = ['source']
    fields = ('datetime', 'value', 'source')

@admin.register(LevelData)
class LevelDataModelAdmin(admin.ModelAdmin):
    list_display = ['datetime', 'value', 'source']
    autocomplete_fields = ['source']
    fields = ('datetime', 'value', 'source')

@admin.register(DischargeData)
class DischargeDataModelAdmin(admin.ModelAdmin):
    list_display = ['datetime', 'value', 'source']
    autocomplete_fields = ['source']
    fields = ('datetime', 'value', 'source')

@admin.register(DischargeDeterministicForecastData)
class DischargeDeterministicForecastDataModelAdmin(admin.ModelAdmin):
    list_display = ['datetime', 'value', 'source']
    autocomplete_fields = ['source']
    fields = ('datetime', 'value', 'source')

@admin.register(DischargeProbabilisticForecastData)
class DischargeProbabilisticForecastDataModelAdmin(admin.ModelAdmin):
    list_display = ['datetime', 'value', 'source']
    autocomplete_fields = ['source']
    fields = ('datetime', 'value', 'source')

# Register your models here.
