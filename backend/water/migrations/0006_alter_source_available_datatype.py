# Generated by Django 4.1.6 on 2023-02-08 09:56

from django.db import migrations, models
import django_better_admin_arrayfield.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('water', '0005_alter_source_available_datatype'),
    ]

    operations = [
        migrations.AlterField(
            model_name='source',
            name='available_datatype',
            field=django_better_admin_arrayfield.models.fields.ArrayField(base_field=models.CharField(choices=[('TEMPERATURE', 'TemperatureData'), ('LEVEL', 'LevelData'), ('DISCHARGE', 'DischargeData'), ('DETERMINISTIC_FORECAST', 'DischargeDeterministicForecastData'), ('PROBABILISTIC_FORECAST', 'DischargeProbabilisticForecastData')], max_length=64), default=['TEMPERATURE', 'LEVEL', 'DISCHARGE', 'DETERMINISTIC_FORECAST', 'PROBABILISTIC_FORECAST'], size=None),
        ),
    ]
