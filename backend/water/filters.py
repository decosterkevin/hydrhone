
from django_filters import rest_framework as filters
from water.models import DischargeProbabilisticForecastData, DischargeDeterministicForecastData, LevelData, DischargeData, TemperatureData

class DischargeProbabilisticForecastDataFilterSet(filters.FilterSet):
    start = filters.DateFilter(field_name="datetime", lookup_expr='gte')
    end = filters.DateFilter(field_name="datetime", lookup_expr='lte')

    class Meta:
        model = DischargeProbabilisticForecastData
        fields = 'start', 'end',

class DischargeDeterministicForecastDataFilterSet(filters.FilterSet):
    start = filters.DateFilter(field_name="datetime", lookup_expr='gte')
    end = filters.DateFilter(field_name="datetime", lookup_expr='lte')
    class Meta:
        model = DischargeDeterministicForecastData
        fields = "start", "end",

class LevelDataFilterSet(filters.FilterSet):
    start = filters.DateFilter(field_name="datetime", lookup_expr='gte')
    end = filters.DateFilter(field_name="datetime", lookup_expr='lte')
    class Meta:
        model = LevelData
        fields = "start", "end",

class DischargeDataFilterSet(filters.FilterSet):
    start = filters.DateFilter(field_name="datetime", lookup_expr='gte')
    end = filters.DateFilter(field_name="datetime", lookup_expr='lte')
    class Meta:
        model = DischargeData
        fields = "start", "end",

class TemperatureDataFilterSet(filters.FilterSet):
    start = filters.DateFilter(field_name="datetime", lookup_expr='gte')
    end = filters.DateFilter(field_name="datetime", lookup_expr='lte')
    class Meta:
        model = TemperatureData
        fields = "start", "end",
