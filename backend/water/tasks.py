from water.models import Source
from django.apps import apps
from celery import shared_task
from water.models import import_source_and_create_objects
# www.hydrodaten.admin.ch importer

@shared_task
def data_import():
    for source in Source.objects.filter(is_active=True):
        import_source_and_create_objects.delay(source.id)

