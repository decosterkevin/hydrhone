from rest_framework import serializers
from .models import SubscriptionRequest

class SubscriptionRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = SubscriptionRequest
        fields = ("id","email","name")