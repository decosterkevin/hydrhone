from django.db import models

# Create your models here.

class SubscriptionRequest(models.Model):
    name = models.CharField(max_length=128, default="")
    email = models.EmailField(db_index=True, unique=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name = "Subscription"
        verbose_name_plural = "Subscription"