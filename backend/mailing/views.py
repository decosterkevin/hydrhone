from django.shortcuts import render
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework import status
from rest_framework.decorators import api_view,authentication_classes, permission_classes
from rest_framework.response import Response
from .serializers import SubscriptionRequestSerializer
from .models import SubscriptionRequest
from django.conf import settings
from django.template.loader import render_to_string
from .tasks import send_email
# Create your views here.

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def contact(request):
    data = request.data
    from_mail = data['email']
    name = data['name']
    client_msg = data['msg']
    
    current_site = settings.DOMAIN
    message = render_to_string('contact_email.html', {
                'client_name': name, 
                'domain':current_site,
                'client_email': from_mail,
                'message':client_msg
            })
    send_email.delay("new contact message (Rhona)", message, settings.CONTACT_EMAIL, from_mail)
    
    return Response({"state":"Message sent to redis"}, status=status.HTTP_200_OK)

class SubscriptionRequestView(CreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = SubscriptionRequestSerializer
    queryset = SubscriptionRequest.objects.all()