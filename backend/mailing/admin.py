from django.contrib import admin
from .models import SubscriptionRequest

@admin.register(SubscriptionRequest)
class SubscriptionRequestModelAdmin(admin.ModelAdmin):
    list_display = ['name', 'email', 'is_active']
    search_fields = ("name", "email")