from django.contrib import admin
from django.contrib.gis import admin as admin_gis
from .models import Alert

@admin.register(Alert)
class AlertModelAdmin(admin.ModelAdmin):
    list_display = ['start', 'end', 'event_title', 'event_type']
