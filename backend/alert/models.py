from django.db import models
from django.db.models.signals import post_save
from mailing.tasks import send_email
from mailing.models import SubscriptionRequest
from django.template.loader import render_to_string
from django.dispatch import receiver
from dynamic_preferences.registries import global_preferences_registry

class FieldsMixin:
    @classmethod
    def get_fields_name(cls):
        return [field.name for field in cls._meta.get_fields()]


class Alert(FieldsMixin, models.Model):
    class Type(models.TextChoices):
        WEATHER = "WEATHER", "Weather"
        WATER = "WATER", "Water"

    sender_name = models.CharField(max_length=256)
    event_type = models.CharField(
        max_length=64, choices=Type.choices, default=Type.WATER
    )
    event_title = models.CharField(max_length=256)
    start = models.DateTimeField()
    end = models.DateTimeField()
    description = models.TextField(default="")

    class Meta:
        unique_together = ("sender_name", "event_type", "event_title", "start", "end")


@receiver(post_save, sender=Alert)
def post_save_alert(sender, instance, created, **kwargs):
    global_preferences = global_preferences_registry.manager()
    mailing_activate = global_preferences["general__activate_alert_mailing"]

    if created and mailing_activate:
        emails = SubscriptionRequest.objects.filter(is_active=True)
        for email in emails:
            message = render_to_string(
                "alert_email.html", {"client_name": name, "alert": instance}
            )
            send_email.delay(
                "new contact message (Rhona)",
                message,
                settings.CONTACT_EMAIL,
                from_mail,
            )

