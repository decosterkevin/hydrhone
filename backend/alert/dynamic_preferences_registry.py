from dynamic_preferences.types import BooleanPreference
from dynamic_preferences.preferences import Section
from dynamic_preferences.registries import global_preferences_registry
from dynamic_preferences.users.registries import user_preferences_registry

general_section = Section("general")

# We start with a global preference
@global_preferences_registry.register
class ActivateAlertMailing(BooleanPreference):
    section = general_section
    name = 'activate_alert_mailing'
    default = False
    required = True