from django.urls import path, include
from rest_framework.routers import DefaultRouter
from alert import views

router = DefaultRouter()
router.register(r'alerts', views.AlertViewSet, basename="alerts")

urlpatterns = [
   path('', include(router.urls)),
]