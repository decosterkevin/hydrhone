
from rest_framework import serializers
from .models import Alert

class AlertSerializer(serializers.ModelSerializer):
    class Meta:
        model = Alert
        fields = (
            "id",
            "sender_name",
            "event_type",
            "event_title",
            "start",
            "end",
            "description",
        )