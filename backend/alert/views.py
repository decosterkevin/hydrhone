from .serializers import AlertSerializer
from .models import Alert
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.generics import  RetrieveAPIView, ListAPIView
from rest_framework.permissions import AllowAny, AllowAny
from rest_framework import viewsets
from rest_framework import filters

class AlertViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = AlertSerializer
    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]
    ordering_fields = ["start", "end"]
    queryset = Alert.objects.all()
    filterset_fields = {
        "start": ['gte', 'gt', 'lte', 'lt'], 
        'end':['lte', 'lt', 'gte', 'gt'],
    }
