from weather.serializers import SourceSerializer, WeatherDataSerializer, DailyForecastWeatherDataSerializer, HourlyForecastWeatherDataSerializer
from weather.models import Source, WeatherData, DailyForecastWeatherData, HourlyForecastWeatherData
from django_filters.rest_framework import DjangoFilterBackend
from weather.filters import DailyForecastWeatherDataFilterSet
from rest_framework.generics import  RetrieveAPIView, ListAPIView
from rest_framework.permissions import AllowAny, AllowAny
from rest_framework import viewsets
from django.db.models import Subquery, FloatField, ExpressionWrapper, OuterRef, F
from rest_framework_gis.filters import InBBoxFilter
from rest_framework import filters
from django.db.models.functions import ExtractDay
from django.db.models.functions import Cast
from django.db.models.fields import DateField
from rest_framework.response import Response
from rest_framework.decorators import action

class SourceViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = SourceSerializer
    filter_backends = [InBBoxFilter, DjangoFilterBackend]
    bbox_filter_field = 'coordinate'
    filterset_fields = [ "title", "is_active", "updated_at", "source_provider"]
    queryset = Source.objects.all()

    @action(detail=True, methods=['get'])
    def current(self, request, pk=None):
        data = WeatherData.objects.filter(source=pk).latest("datetime")
        temperature_max = HourlyForecastWeatherData.objects.filter(source=pk, datetime__date=data.datetime.date()).order_by("-temperature").first()
        temperature_min = HourlyForecastWeatherData.objects.filter(source=pk, datetime__date=data.datetime.date()).order_by("temperature").first()
        data.temperature_max = temperature_max.temperature
        data.temperature_max_datetime = temperature_max.datetime
        data.temperature_min = temperature_min.temperature
        data.temperature_min_datetime = temperature_min.datetime
        serializer = WeatherDataSerializer(data)
        return Response(serializer.data)


class WeatherDataViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = WeatherDataSerializer
    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]
    ordering_fields = ["datetime"]
    queryset = WeatherData.objects.all()
    filterset_fields = {
        'datetime':['gte', 'lte', 'exact', 'gt', 'lt'],
    }
    def get_queryset(self):
        return WeatherData.objects.annotate(
            date_only=Cast('datetime', DateField()),
            temperature_max = Subquery(
                HourlyForecastWeatherData.objects.filter(
                    source=OuterRef("source"), 
                    datetime__date=OuterRef("date_only")
                ).order_by("-temperature").values("temperature")[:1]
            ),
            temperature_max_datetime = Subquery(
                HourlyForecastWeatherData.objects.filter(
                    source=OuterRef("source"), 
                    datetime__date=OuterRef("date_only")
                ).order_by("-temperature").values("datetime")[:1]
            ),
            temperature_min = Subquery(
                HourlyForecastWeatherData.objects.filter(
                    source=OuterRef("source"), 
                    datetime__date=OuterRef("date_only")
                ).order_by("temperature").values("temperature")[:1]
            ),
            temperature_min_datetime = Subquery(
                HourlyForecastWeatherData.objects.filter(
                    source=OuterRef("source"), 
                    datetime__date=OuterRef("date_only")
                ).order_by("temperature").values("datetime")[:1]
            )
        )

class WeatherDataSourceViewSet(WeatherDataViewSet):
    filterset_fields = {
        'datetime':['gte', 'lte', 'exact', 'gt', 'lt'],
    }
    def get_queryset(self):
        source_id = self.kwargs['source_id']
        return super().get_queryset().filter(source__id=source_id)

class DailyForecastWeatherDataViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = DailyForecastWeatherDataSerializer
    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]
    queryset = DailyForecastWeatherData.objects.all()
    filter_class = DailyForecastWeatherDataFilterSet
    def get_queryset(self):
        return DailyForecastWeatherData.objects.annotate(
            temperature = (F('temperature_max') - F('temperature_min'))/2,
            temperature_felt = (F('temperature_felt_evening') - F('temperature_felt_morning'))/2
        )
class DailyForecastWeatherDataSourceViewSet(DailyForecastWeatherDataViewSet):
    def get_queryset(self):
        source_id = self.kwargs['source_id']
        return super().get_queryset().filter(source__id=source_id)

class HourlyForecastWeatherDataViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = HourlyForecastWeatherDataSerializer
    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]
    ordering_fields = ["datetime"]
    queryset = HourlyForecastWeatherData.objects.all()
    filterset_fields = {
        'datetime':['gte', 'lte', 'exact', 'gt', 'lt'],
    }

class HourlyForecastWeatherDataSourceViewSet(HourlyForecastWeatherDataViewSet):
    filterset_fields = {
        'datetime':['gte', 'lte', 'exact', 'gt', 'lt'],
    }
    def get_queryset(self):
        source_id = self.kwargs['source_id']
        return super().get_queryset().filter(source__id=source_id)
