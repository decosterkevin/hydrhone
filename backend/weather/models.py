from django.db import models
from django.contrib.gis.db.models import PointField
from django.contrib.gis.geos import Point
from django.db.models.signals import post_save
from celery import shared_task
from dynamic_preferences.registries import global_preferences_registry
from django.conf import settings
from django.dispatch import receiver

class FieldsMixin:
    @classmethod
    def get_fields_name(cls):
        return [field.name for field in cls._meta.get_fields()]

class Source(models.Model):
    class Provider(models.TextChoices):
        OPEN_WEATHER = "OPEN_WEATHER", "Open Weather"

    title = models.CharField(max_length=256)
    coordinate = PointField(blank=True, null=True)
    is_active = models.BooleanField(default=True)
    source_provider = models.CharField(max_length=32, choices=Provider.choices, default=Provider.OPEN_WEATHER)
    fields_map = models.JSONField(default=dict)
    updated_at = models.DateTimeField(blank=True, null=True)
    def __str__(self):
        return f'{self.title}'

    class Meta:
        verbose_name = "Source"
        verbose_name_plural = "Sources"

class WeatherData(FieldsMixin, models.Model):
    source = models.ForeignKey("weather.Source", related_name="weather_data", on_delete=models.CASCADE)
    datetime = models.DateTimeField()
    sunrise_datetime = models.DateTimeField(blank=True, null=True)
    sunset_datetime = models.DateTimeField(blank=True, null=True)
    temperature = models.FloatField()
    temperature_felt = models.FloatField(blank=True, null=True)
    pressure = models.FloatField(blank=True, null=True)
    humidity = models.FloatField(blank=True, null=True)
    atmospheric_temperature = models.FloatField(blank=True, null=True)
    clouds = models.FloatField(blank=True, null=True)
    uv_index = models.FloatField(blank=True, null=True)
    rain_volume = models.FloatField(blank=True, null=True)
    snow_volume = models.FloatField(blank=True, null=True)
    visibility = models.FloatField(blank=True, null=True)
    wind_speed = models.FloatField(blank=True, null=True)
    wind_direction = models.FloatField(blank=True, null=True)
    weather_icon = models.URLField(blank=True, null=True)
    weather_description = models.CharField(max_length=126, default="")
    weather_title = models.CharField(max_length=126, default="")
    weather_id = models.IntegerField(blank=True, null=True)

    
    class Meta:
        unique_together = ["source", "datetime"]
        verbose_name = "Current Weather Data"
        verbose_name_plural = "Current Weather Data"
        ordering = ['-datetime']
    
class HourlyForecastWeatherData(FieldsMixin, models.Model):
    source = models.ForeignKey("weather.Source", related_name="hourly_forecast_weather_data", on_delete=models.CASCADE)
    datetime = models.DateTimeField()
    temperature = models.FloatField()
    temperature_felt = models.FloatField(blank=True, null=True)
    pressure = models.FloatField(blank=True, null=True)
    humidity = models.FloatField(blank=True, null=True)
    atmospheric_temperature = models.FloatField(blank=True, null=True)
    clouds = models.FloatField(blank=True, null=True)
    uv_index = models.FloatField(blank=True, null=True)
    rain_volume = models.FloatField(blank=True, null=True)
    snow_volume = models.FloatField(blank=True, null=True)
    visibility = models.FloatField(blank=True, null=True)
    wind_speed = models.FloatField(blank=True, null=True)
    wind_direction = models.FloatField(blank=True, null=True)
    weather_icon = models.CharField(max_length=32,blank=True, null=True)
    weather_description = models.CharField(max_length=126, default="")
    weather_title = models.CharField(max_length=126, default="")
    weather_id = models.IntegerField(blank=True, null=True)
    
    probability_rain = models.FloatField(blank=True, null=True)

    class Meta:
        verbose_name = "Hourly Forecast"
        verbose_name_plural = "Hourly Forecasts"
        ordering = ['-datetime']

class DailyForecastWeatherData(FieldsMixin, models.Model):
    source = models.ForeignKey("weather.Source", related_name="daily_forecast_weather_data", on_delete=models.CASCADE)
    datetime = models.DateTimeField()
    sunrise_datetime = models.DateTimeField(blank=True, null=True)
    sunset_datetime = models.DateTimeField(blank=True, null=True)

    pressure = models.FloatField(blank=True, null=True)
    humidity = models.FloatField(blank=True, null=True)
    atmospheric_temperature = models.FloatField(blank=True, null=True)
    clouds = models.FloatField(blank=True, null=True)
    uv_index = models.FloatField(blank=True, null=True)
    rain_volume = models.FloatField(blank=True, null=True)
    snow_volume = models.FloatField(blank=True, null=True)
    visibility = models.FloatField(blank=True, null=True)
    wind_speed = models.FloatField(blank=True, null=True)
    wind_direction = models.FloatField(blank=True, null=True)
    weather_icon = models.URLField(blank=True, null=True)
    weather_description = models.CharField(max_length=126, default="")
    weather_title = models.CharField(max_length=126, default="")
    weather_id = models.IntegerField(blank=True, null=True)

    temperature_morning = models.FloatField()
    temperature_evening = models.FloatField()
    temperature_night = models.FloatField()
    temperature_min = models.FloatField()
    temperature_max = models.FloatField()
    temperature_felt_morning = models.FloatField(blank=True, null=True)
    temperature_felt_day = models.FloatField(blank=True, null=True)
    temperature_felt_evening = models.FloatField(blank=True, null=True)
    temperature_felt_night = models.FloatField(blank=True, null=True)
    probability_rain = models.FloatField(blank=True, null=True)
    
    class Meta:
        verbose_name = "Daily Forecast"
        verbose_name_plural = "Daily Forecasts"
        ordering = ['-datetime']