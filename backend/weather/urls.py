from django.urls import path, include
from rest_framework.routers import DefaultRouter
from weather import views

router = DefaultRouter()
router.register(r'sources', views.SourceViewSet, basename="sources")
router.register(r'data', views.WeatherDataViewSet, basename="data")
router.register(r'daily_forecast_data', views.DailyForecastWeatherDataViewSet, basename="daily_forecast_data")
router.register(r'hourly_forecast_data', views.HourlyForecastWeatherDataViewSet, basename="hourly_forecast_data")

source_router = DefaultRouter()
source_router.register(r'data', views.WeatherDataSourceViewSet, basename="sources-data")
source_router.register(r'daily_forecast_data', views.DailyForecastWeatherDataSourceViewSet, basename="sources-daily_forecast_data")
source_router.register(r'hourly_forecast_data', views.HourlyForecastWeatherDataSourceViewSet, basename="sources-hourly_forecast_data")

urlpatterns = [
   path('', include(router.urls)),
   path('sources/<int:source_id>/', include(source_router.urls)),
]