
from django_filters import rest_framework as filters
from weather.models import DailyForecastWeatherData, HourlyForecastWeatherData, WeatherData

class DailyForecastWeatherDataFilterSet(filters.FilterSet):
    start = filters.DateFilter(field_name="datetime", lookup_expr='gte')
    end = filters.DateFilter(field_name="datetime", lookup_expr='lte')

    class Meta:
        model = DailyForecastWeatherData
        fields = 'start', 'end',

class HourlyForecastWeatherDataFilterSet(filters.FilterSet):
    start = filters.DateFilter(field_name="datetime", lookup_expr='gte')
    end = filters.DateFilter(field_name="datetime", lookup_expr='lte')

    class Meta:
        model = HourlyForecastWeatherData
        fields = 'start', 'end',

class WeatherDataFilterSet(filters.FilterSet):
    start = filters.DateFilter(field_name="datetime", lookup_expr='gte')
    end = filters.DateFilter(field_name="datetime", lookup_expr='lte')

    class Meta:
        model = WeatherData
        fields = 'start', 'end',
