from django.apps import AppConfig
from django.db.models.signals import post_migrate
from django.dispatch import receiver
from django.contrib.gis.geos import Point

class weatherConfig(AppConfig):
    name = 'weather'

    def ready(self):
        @receiver(post_migrate)
        def _post_migrate(sender,**kwargs):
            from weather.models import Source
            Source.objects.get_or_create(
                source_provider = Source.Provider.OPEN_WEATHER,
                defaults={
                    "title": "Geneva",
                    "coordinate": Point(6.0858841000977, 46.213129068848),
                    "is_active": True,
                    "fields_map": {'dt': 'datetime', 'end': 'end', 'pop': 'probability_rain', 'uvi': 'uv_index', 'rain': 'rain_volume', 'snow': 'snow_volume', 'temp': 'temperature', 'event': 'event_title', 'start': 'start', 'clouds': 'clouds', 'sunset': 'sunset_datetime', 'rain_1h': 'rain_volume', 'snow_1h': 'snow_volume', 'sunrise': 'sunrise_datetime', 'humidity': 'humidity', 'pressure': 'pressure', 'temp_day': 'temperature', 'temp_eve': 'temperature_evening', 'temp_max': 'temperature_max', 'temp_min': 'temperature_min', 'wind_deg': 'wind_direction', 'dew_point': 'atmospheric_temperature', 'temp_morn': 'temperature_morning', 'feels_like': 'temperature_felt', 'temp_night': 'temperature_night', 'visibility': 'visibility', 'weather_id': 'weather_id', 'wind_speed': 'wind_speed', 'description': 'description', 'event_title': 'event', 'sender_name': 'sender_name', 'weather_icon': 'weather_icon', 'weather_main': 'weather_title', 'feels_like_day': 'temperature_felt_day', 'feels_like_eve': 'temperature_felt_evening', 'feels_like_morn': 'temperature_felt_morning', 'feels_like_night': 'temperature_felt_night', 'weather_description': 'weather_description'}
                }
            )