from django.contrib import admin
from django.contrib.gis import admin as admin_gis
from .models import Source, WeatherData, DailyForecastWeatherData, HourlyForecastWeatherData

class LocationAdmin(admin_gis.GeoModelAdmin):
    default_lat=46.204546
    default_lon=6.143219

@admin.register(WeatherData)
class WeatherDataModelAdmin(admin.ModelAdmin):
    list_display = ['datetime', 'source']
    autocomplete_fields = ['source']

@admin.register(DailyForecastWeatherData)
class DailyForecastWeatherDataModelAdmin(admin.ModelAdmin):
    list_display = ['datetime', 'source']
    autocomplete_fields = ['source']

@admin.register(HourlyForecastWeatherData)
class HourlyForecastWeatherDataModelAdmin(admin.ModelAdmin):
    list_display = ['datetime', 'source']
    autocomplete_fields = ['source']

@admin.register(Source)
class SourceModelAdmin(LocationAdmin):
    list_display = ['title', 'is_active', 'source_provider' ]
    search_fields = ("title",)
    fields = ["title", "coordinate", "is_active", "source_provider", "fields_map"]


# Register your models here.
