from weather.models import Source
from django.apps import apps
from celery import shared_task
from weather.models import (
    WeatherData,
    DailyForecastWeatherData,
    HourlyForecastWeatherData
)
from alert.models import Alert as WeatherAlert
from weather.importer import OpenWeatherAPIProvider
from django.utils import timezone
from dynamic_preferences.registries import global_preferences_registry
from django.conf import settings
from django.db.models import Q
# www.hydrodaten.admin.ch importer


@shared_task
def import_source_and_create_objects(source_id):
    global_preferences = global_preferences_registry.manager()
    source = Source.objects.get(id=source_id)
    url = global_preferences["general__weather_data_provider_url"].format(
        source.coordinate.y, source.coordinate.x, settings.OPEN_WEATHER_API_KEY
    )
    provider = OpenWeatherAPIProvider(url, source.fields_map)
    provider.init()
    
    weather_models = [
        WeatherData(source=source, **obj)
        for obj in provider.get_normalized_current_data(WeatherData.get_fields_name())
    ]
    WeatherData.objects.bulk_create(weather_models, ignore_conflicts=True)

    for obj in provider.get_normalized_forecast_daily_data(DailyForecastWeatherData.get_fields_name()):
        DailyForecastWeatherData.objects.update_or_create(source=source, datetime=obj['datetime'], defaults=obj)

    for obj in provider.get_normalized_forecast_hourly_data(HourlyForecastWeatherData.get_fields_name()):
        HourlyForecastWeatherData.objects.update_or_create(source=source, datetime=obj['datetime'], defaults=obj)

    for obj in provider.get_normalized_alerts_data(WeatherAlert.get_fields_name()):
        alerts = WeatherAlert.objects.filter(event_type=WeatherAlert.Type.WEATHER.name, sender_name=obj['sender_name'], event_title=obj['event_title'])
        alerts = alerts.filter(Q(start__lte=obj['end']) & Q(end__gte=obj['start']))
        if alerts.exists():
            alerts.update(start=obj['start'], end=obj['end'], description=obj['description'])
        else:
            WeatherAlert.objects.create(event_type=WeatherAlert.Type.WEATHER.name, **obj)

    source.updated_at = timezone.now()
    source.save()

@shared_task
def import_source():
    for source in Source.objects.filter(is_active=True):
        print(f"Processing source {source.title}")
        import_source_and_create_objects.delay(source.id)
