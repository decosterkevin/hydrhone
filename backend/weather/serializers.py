
from rest_framework import serializers
from weather.models import Source, WeatherData, HourlyForecastWeatherData, DailyForecastWeatherData

class SourceSerializer(serializers.ModelSerializer):
    temperature_min = serializers.FloatField(default=0)
    temperature_min_datetime = serializers.DateTimeField(default=None)
    class Meta:
        model = Source
        fields = ("id", "title", "coordinate", "updated_at", "is_active", "source_provider")

class WeatherDataSerializer(serializers.ModelSerializer):
    temperature_max = serializers.FloatField(default=0)
    temperature_min = serializers.FloatField(default=0)
    temperature_min_datetime = serializers.DateTimeField(default=None)
    temperature_max_datetime = serializers.DateTimeField(default=None)
    class Meta:
        model = WeatherData
        fields = (
            "datetime",
            "sunrise_datetime",
            "sunset_datetime",
            "temperature",
            "temperature_max",
            "temperature_min",
            "temperature_min_datetime",
            "temperature_max_datetime",
            "temperature_felt",
            "humidity",
            "clouds",
            "wind_speed",
            "weather_icon",
            "weather_id",
            "weather_description"
        )

class HourlyForecastWeatherDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = HourlyForecastWeatherData
        fields = (
            "datetime",
            "temperature",
            "weather_icon",
            "weather_id",
            "weather_description"
        )

class DailyForecastWeatherDataSerializer(serializers.ModelSerializer):
    temperature = serializers.FloatField(read_only=True)
    temperature_felt = serializers.FloatField(read_only=True)
    class Meta:
        model = DailyForecastWeatherData
        fields = (
            "datetime",
            "sunrise_datetime",
            "sunset_datetime",
            "humidity",
            "clouds",
            "wind_speed",
            "weather_icon",
            "weather_id",
            "weather_description",
            "temperature",
            "temperature_min",
            "temperature_max",
            "temperature_felt",
            "probability_rain",
        )