# Generated by Django 3.1.4 on 2021-01-06 10:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('weather', '0009_auto_20210106_0906'),
    ]

    operations = [
        migrations.DeleteModel(
            name='WeatherAlerts',
        ),
    ]
