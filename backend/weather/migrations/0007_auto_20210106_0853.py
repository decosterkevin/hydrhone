# Generated by Django 3.1.4 on 2021-01-06 07:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('weather', '0006_auto_20210106_0852'),
    ]

    operations = [
        migrations.AlterField(
            model_name='source',
            name='fields_map',
            field=models.JSONField(default=dict),
        ),
    ]
