# Generated by Django 3.1.4 on 2021-01-06 08:06

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('weather', '0008_auto_20210106_0859'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='weatheralerts',
            unique_together={('source', 'sender_name', 'event_title', 'start', 'end')},
        ),
    ]
