import pandas as pd
import requests
class OpenWeatherAPIProvider:

    def __init__(self, _url, _map):
        self.url = _url
        self.map = _map
        self.current_weather_df = None
        self.daily_forecasts_df = pd.DataFrame()
        self.hourly_forecasts_df = pd.DataFrame()
        self.alerts_df = pd.DataFrame()
        self.timezone = ""

    @staticmethod
    def _construct_df(data):
        if isinstance(data, list):
            for l in data:
                l["weather"] = l.get("weather", [{}])[0]
        else:
            data["weather"] = data.get("weather", [{}])[0]
        df = pd.json_normalize(data, sep="_")
        if 'dt' in df.columns:
            df['dt'] = pd.to_datetime(df['dt'], unit='s', utc=True)
        if 'start' in df.columns:
            df['start'] = pd.to_datetime(df['start'], unit='s', utc=True)
        if 'end' in df.columns:
            df['end'] = pd.to_datetime(df['end'], unit='s', utc=True)
        if "sunrise" in df.columns:
            df['sunrise'] = pd.to_datetime(df['sunrise'], unit='s', utc=True)
        if "sunset" in df.columns:
            df['sunset'] = pd.to_datetime(df['sunset'], unit='s', utc=True)
        return df

    def init(self):
        headers = {
            'user-agent': 'Rhone.io',
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
        response = requests.get(self.url, headers=headers)
        if response.status_code == requests.codes.ok:
            r_json = response.json()
            self.timezone = r_json['timezone']
            if r_json.get("current", None):
                self.current_weather_df =  self._construct_df(r_json['current'])
            if r_json.get("daily", None):
                self.daily_forecasts_df =  self._construct_df(r_json['daily'])
            if r_json.get("hourly", None):
                self.hourly_forecasts_df =  self._construct_df(r_json['hourly'])
            if r_json.get("alerts", None):
                self.alerts_df =  self._construct_df(r_json['alerts'])

    def get_normalized_current_data(self, valid_fields):
        df = self.current_weather_df.rename(columns=self.map)
        df = df.drop(columns=df.columns.difference(valid_fields))
        df = df.where(pd.notnull(df), None)
        return df.to_dict("records")

    def get_normalized_forecast_daily_data(self, valid_fields):
        df = self.daily_forecasts_df.rename(columns=self.map)
        df = df.where(pd.notnull(df), None)
        df = df.drop(columns=df.columns.difference(valid_fields))
        return df.to_dict("records")

    def get_normalized_forecast_hourly_data(self, valid_fields):
        df = self.hourly_forecasts_df.rename(columns=self.map)
        df = df.drop(columns=df.columns.difference(valid_fields))
        df = df.where(pd.notnull(df), None)
        return df.to_dict("records")

    def get_normalized_alerts_data(self, valid_fields):
        df = self.alerts_df.rename(columns=self.map)
        df = df.drop(columns=df.columns.difference(valid_fields))
        df = df.where(pd.notnull(df), None)
        return df.to_dict("records")