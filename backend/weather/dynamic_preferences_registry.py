from dynamic_preferences.types import BooleanPreference, StringPreference
from dynamic_preferences.preferences import Section
from dynamic_preferences.registries import global_preferences_registry
from dynamic_preferences.users.registries import user_preferences_registry

general_section = Section("general")

# We start with a global preference
@global_preferences_registry.register
class WeatherDataProviderUrl(StringPreference):
    section = general_section
    name = 'weather_data_provider_url'
    default = 'https://api.openweathermap.org/data/2.5/onecall?lat={0}&lon={1}&units=metric&exclude=minutely&appid={2}'
    required = True